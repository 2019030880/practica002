package modelo;

public class Cotizaciones {
    private int numCotizacion;
    private String descripcion;
    private int precio;
    private int porcentaje;
    private int plazo;
    
    public Cotizaciones(){
        this.descripcion="";
        this.numCotizacion=0;
        this.plazo=0;
        this.porcentaje=0;
        this.precio=0;
    }
    
    public Cotizaciones(int numCotizacion, String descripcion, int precio, int porcentaje, int plazo){
        this.descripcion=descripcion;
        this.numCotizacion=numCotizacion;
        this.plazo=plazo;
        this.porcentaje=porcentaje;
        this.precio=precio;
    }
    
    public Cotizaciones(Cotizaciones aux){
        this.descripcion = aux.descripcion;
        this.numCotizacion = aux.numCotizacion;
        this.plazo = aux.plazo;
        this.porcentaje = aux.porcentaje;
        this.precio = aux.precio;
    }
    
    public void setNumCoti(){
        this.numCotizacion= numCotizacion+1;
    }
    
    public int getNumCoti(){
        return numCotizacion;
    }
    
    public void setDescripcion(String des){
        this.descripcion = des;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public void setPrecio(int pre){
        this.precio=pre;
    }
    
    public int getPrecio(){
        return precio;
    }
    
    public void setPorcentaje(int porCiento){
        this.porcentaje=porCiento;
    }
    
    public int getPorcentaje(){
        return porcentaje;
    }
    
    public void setPlazo(int meses){
        this.plazo=meses;
    }
    
    public int getPlazo(){
        return plazo;
    }
    
    public int calcularPagoIni(){
        return (int)(precio*(porcentaje*0.01));
    }
    
    public int calcularPagoRes(){
        return precio - calcularPagoIni();
    }
    
    public int calcularPagoMen(){
        return (int) (calcularPagoRes() / getPlazo());
    }
}
